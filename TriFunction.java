@FunctionalInterface
interface TriFunction<T, U, V, R> {
    R apply(T t, U u, V v);
}

// Example:
//	
// TriFunction <Integer, Integer, Integer, Integer> sum = (a, b, c) -> a + b + c;
//
// System.out.println(sum.apply(a,b,c));